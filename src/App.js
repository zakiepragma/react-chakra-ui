import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CreatePost from "./pages/CreatePost";
import EditPost from "./pages/EditPost";
import ListingPosts from "./pages/ListingPosts";
import ShowingPostDetail from "./pages/ShowingPostDetail";

const App = () => {
  return (
    <BrowserRouter>
      <div>
        <Routes>
          <Route path="/" element={<ListingPosts />} />
          <Route path="/create-post" element={<CreatePost />} />
          <Route path="/edit-post" element={<EditPost />} />
          <Route path="/showing-post-detail" element={<ShowingPostDetail />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
};

export default App;
