import React, { useState } from "react";
import axios from "axios";
import { Container, Flex, Heading, Text, Textarea } from "@chakra-ui/react";
import { Box, FormControl, FormLabel, Input, Button } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CreatePost = () => {
  const [formData, setFormData] = useState({
    title: "",
    body: "",
  });
  const [formErrors, setFormErrors] = useState({
    title: "",
    body: "",
  });
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSubmitting(true);
    // Validasi Form
    let errors = {};
    if (!formData.title) {
      errors.title = "Title is required";
    }
    if (!formData.body) {
      errors.body = "Body is required";
    }
    if (Object.keys(errors).length > 0) {
      setFormErrors(errors);
      setIsSubmitting(false);
    } else {
      axios
        .post("https://jsonplaceholder.typicode.com/posts", formData)
        .then((response) => {
          console.log(response.data);
          setIsSubmitting(false);
          setFormData({
            title: "",
            body: "",
          });
          setFormErrors({
            title: "",
            body: "",
          });
        })
        .catch((error) => {
          console.log(error);
          setIsSubmitting(false);
        });
    }
  };

  return (
    <Container
      display="flex"
      justifyContent="center"
      alignItems="center"
      h="100%"
      marginY={"100"}
      maxW={"100%"}
    >
      <Box w="60%" borderWidth="1px" borderRadius="lg" p={6}>
        <Heading marginBottom={"5"}>Create Post</Heading>
        <form onSubmit={handleSubmit}>
          <FormControl>
            <FormLabel>Title</FormLabel>
            <Input
              type="text"
              id="title"
              name="title"
              value={formData.title}
              onChange={handleInputChange}
              placeholder="Masukkan title"
            />
            {formErrors.title && <Text color="red">{formErrors.title}</Text>}
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>Body</FormLabel>
            <Textarea
              id="body"
              name="body"
              value={formData.body}
              onChange={handleInputChange}
              placeholder="Masukkan body"
            />
            {formErrors.body && <Text color="red">{formErrors.body}</Text>}
          </FormControl>
          <Flex alignItems={"center"} justifyContent={"space-between"}>
            <Button
              mt={4}
              colorScheme="blue"
              type="submit"
              disabled={isSubmitting}
            >
              {isSubmitting ? "Submitting..." : "Submit"}
            </Button>
            <Link to={"/"}>
              <Button mt={4} gap={"2"}>
                <FontAwesomeIcon icon={faHome} /> Back to Home
              </Button>
            </Link>
          </Flex>
        </form>
      </Box>
    </Container>
    // <div>
    //   <form onSubmit={handleSubmit}>
    //     <div>
    //       <label htmlFor="title">Title:</label>
    //       <input
    //         type="text"
    //         id="title"
    //         name="title"
    //         value={formData.title}
    //         onChange={handleInputChange}
    //       />
    //       {formErrors.title && <Text color="red">{formErrors.title}</Text>}
    //     </div>
    //     <div>
    //       <label htmlFor="body">Body:</label>
    //       <textarea
    //         id="body"
    //         name="body"
    //         value={formData.body}
    //         onChange={handleInputChange}
    //       />
    //       {formErrors.body && <Text color="red">{formErrors.body}</Text>}
    //     </div>
    //     <button type="submit" disabled={isSubmitting}>
    //       {isSubmitting ? "Submitting..." : "Submit"}
    //     </button>
    //   </form>
    // </div>
  );
};

export default CreatePost;
