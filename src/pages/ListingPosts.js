import {
  Box,
  Button,
  Container,
  Flex,
  Grid,
  GridItem,
  Heading,
  IconButton,
  Text,
} from "@chakra-ui/react";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCogs, faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const ListingPosts = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((response) => {
        setPosts(response.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  }, []);

  return (
    <Container maxW={"100%"} padding={"10"}>
      <Flex alignItems={"center"} justifyContent={"space-between"}>
        <Heading marginY={"5"}>Listing Posts</Heading>
        <Link to="/create-post">
          <Button>Create Post</Button>
        </Link>
      </Flex>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <Grid
          templateColumns={[
            "repeat(1, 1fr)",
            "repeat(2, 1fr)",
            "repeat(3, 1fr)",
          ]}
          gap={6}
        >
          {posts.map((post) => (
            <GridItem key={post.id}>
              <Box borderWidth="1px" borderRadius="lg" marginBottom={"3"} p={4}>
                <Text fontSize="xl" fontWeight="bold">
                  {post.title}
                </Text>
                <Text mt={2}>{post.body}</Text>
                <Box mt={4} display="flex" gap={"2"}>
                  <IconButton
                    aria-label="Detail"
                    icon={<FontAwesomeIcon icon={faCogs} />}
                    // onClick={onEdit}
                  />
                  <IconButton
                    aria-label="Edit"
                    icon={<FontAwesomeIcon icon={faEdit} />}
                    // onClick={onEdit}
                  />
                  <IconButton
                    aria-label="Delete"
                    icon={<FontAwesomeIcon icon={faTrash} />}
                    // onClick={onDelete}
                  />
                </Box>
              </Box>
            </GridItem>
          ))}
        </Grid>
      )}
    </Container>
  );
};

export default ListingPosts;
